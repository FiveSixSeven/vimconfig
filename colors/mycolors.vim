
"=-------------------------------------------------------------------------------="
" Basic Colors
"=-------------------------------------------------------------------------------="
" 
hi Normal        ctermbg=232    guibg=#272822     guifg=#f8f8f2
hi NonText       ctermfg=232    guibg=#272822     guifg=#282828

hi CursorLine    ctermbg=238    guibg=#272822
hi CursorLineNr  ctermbg=238    guibg=#49483e      guifg=#f8f8f0
"hi CursorLineNr  ctermbg=238    guibg=#21201e      guifg=#9c9c94
hi StatusLine    ctermbg=233    guibg=#1a1a1a   guifg=#bcbcbc gui=NONE
hi StatusLineNC  ctermbg=233    guibg=#1a1a1a   guifg=#383735 gui=NONE
hi LineNr        ctermbg=233    ctermfg=235     guifg=#383735 guibg=#21201e
hi SignColumn    ctermbg=233    guibg=#21201e
hi VertSplit     ctermbg=233    ctermfg=233     guibg=#1a1a1a guifg=#1a1a1a
hi ColorColumn                  guibg=#242321
hi MatchParen guibg=#272822
hi ModeMsg       term=bold cterm=bold ctermfg=222 gui=bold guifg=#00ff00
hi WarningMsg    guibg=#21201e guifg=#ff2c4b


"=-------------------------------------------------------------------------------="
" FileType Colors (modification of syntax highlight groups)
"=-------------------------------------------------------------------------------="
hi pythonConvention guifg=#87ffd7 ctermfg=122
hi Repeat term=bold gui=bold
hi Operator term=bold gui=bold
hi Comment term=italic gui=italic

"=-------------------------------------------------------------------------------="
" GUI specific colors
"=-------------------------------------------------------------------------------="
if has("gui_running")
   hi SpellBad    guisp=#ff2c4b
   hi SpellCap    guisp=#ff2c4b  
   hi SpellLocal  guisp=#ff2c4b  
   hi SpellRare   guisp=#ff2c4b
   hi SyntasticError   gui=undercurl guisp=#fade3e
   hi SyntasticWarning gui=undercurl guisp=#fade3e
   hi PyFlakes         gui=undercurl guisp=#fade3e 
endif

 
"=-------------------------------------------------------------------------------="
" Plugin colors
"=-------------------------------------------------------------------------------="

" -------- Airline --------
hi airline_warning ctermbg=196 guibg=#ff2c4b


" -------- Bufgator --------
hi BuffergatorCurrentEntry guifg=#00ff00
hi BuffergatorAlternateEntry guifg=#ffff00
hi BuffergatorModifiedEntry guifg=#ff0000
"
" -------- MiniBuffer --------
hi MBENormal               guifg=#808080 
hi MBEChanged              guifg=#CD5907 
hi MBEVisibleNormal        guifg=#A6DB29 
hi MBEVisibleChanged       guifg=#F1266F 
hi MBEVisibleActiveNormal  guifg=#A6DB29 
hi MBEVisibleActiveChanged guifg=#F1266F 


" -------- Syntastic --------
hi SyntasticErrorSign ctermbg=208 gui=bold guibg=#21201e guifg=#ff2c4b
hi SyntasticStyleErrorSign ctermbg=208 gui=bold guibg=#21201e guifg=#ff5f00
hi SyntasticWarningSign guibg=#21201e
hi SyntasticStyleWarningSign guibg=#21201e

" ------- TagBar --------
hi link TagbarSignature Comment
