" My vimrc Settings
"
" Sections (search for => )
"  1) General Settings
"  2) User Interface Settings
"  3) Text and Indent Settings
"  4) Search and Menu Settings
"  5) Motion Mappings
"  6) Editing Mappings
"  7) Misc. Mappings
"  8) Personal Commands
"  9) Auto and Filetype Commands
" 10) Colors
" 11) VIM Plugins
"

"=------------------------------------------------------------------------------="
" => General Settings
"=------------------------------------------------------------------------------="
  
" Filetype commands
filetype plugin on
filetype indent on

" Make vim incompatible to vi.
set nocompatible
set modelines=0

" Use UTF-8
set encoding=utf-8
"Changing Leader Key
let mapleader = ","

" Current shell
set shell=/bin/bash

" Enable Mouse
set mouse=a

" Set to auto read when a file is changed from the outside
"set autoread

" Disable swap files
set noswapfile

" Backups different then swaps!
" Skip Backups for these files
set backupskip=/tmp/*,/private/tmp/*"

" Dictionary path, from which the words are being looked up.
set dictionary=/usr/share/dict/words

"=------------------------------------------------------------------------------="
" => User Interface Settings
"=------------------------------------------------------------------------------="
 
" Delay redrawing
set lazyredraw

" Pixels between lines
set linespace=2

" Set title to window
set title

" Status line (2 = always)
set laststatus=2

" Show the current mode in statusline
set showmode

" Show cursor line/column number
set ruler

" Show the last command
set showcmd

" Number of lines always above/below curosr
set scrolloff=3

" unloaded buffers are abandoned
set hidden

" For fast terminal connections (NOT for remote shells)
set ttyfast

" Show matching brackets
set showmatch
" How many tenths of a second to show brackets
set matchtime=3

" Highlight the cursor line (color may be changed)
set cursorline

" Line numbers in gutter
set number
set norelativenumber

" Pop-up-menu and completion options
set completeopt=menu,longest,preview
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

" Display color column (if available)
" if exists("&colorcolumn")
"    set colorcolumn=100
" endif

" GUI settings
if has("gui_running")
    " Font info
    set guifont=LucidaMonoEFForPowerline\ 14

    " GUI Cursor control
    set guicursor=a:blinkon0

    " GUI window label
    set guitablabel=%-0.12t%M

    " GUI interface options
    set guioptions-=T
    set guioptions-=r
    set guioptions-=L
    set guioptions+=a
    "Set guioptions-=m

    " Invisibles using the Textmate style
    set listchars=tab:▸\ ,eol:¬         

    set lines=40 columns=175
"Console Setting
else
    " Use 256 colors
    set t_Co=256

   " Resize Split When the window is resized"
   au VimResized * :wincmd =
   set vb t_vb=
endif

"=------------------------------------------------------------------------------="
" => Text, Editting, and Indent Settings
"=------------------------------------------------------------------------------="
 
" Configure backspace <BS>
set backspace=indent,eol,start

" Tabs replaced by spaces
set expandtab

set tabstop=8
set shiftwidth=4
set softtabstop=4

" auto indenent lines
set autoindent

" Pasting without indentation"
set pastetoggle=<F3>
 
" Allow for selection of Visual block past last charaacter
set virtualedit=block

" Wrap excessive lines after textwidth, with formatoptions
"set wrap
"set textwidth=100
"set formatoptions=qrn1

"=------------------------------------------------------------------------------="
" => Search and Menu Settings
"=------------------------------------------------------------------------------="

"Settings for Searching and Moving
nnoremap / /\v
vnoremap / /\v

" Ignore case
set ignorecase

" Ignore case
set smartcase

" Highlight last search 
set hlsearch

" Show matches as typing
set incsearch

" Allow for global substitution by default
" Note: if a 'g' is not supplied in search it will
" toggle global OFF
set gdefault

" Wildmenu completion (TAB in ':' commands)
set wildmenu

" Wildmenu completion mode
set wildmode=list:longest

" Wildmenu ignored list 
set wildignore+=.hg,.git,.svn " Version Controls"
set wildignore+=*.aux,*.out,*.toc "Latex Indermediate files"
set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg "Binary Imgs"
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest "Compiled Object files"
set wildignore+=*.spl "Compiled spelling world list"
set wildignore+=*.sw? "Vim swap files"
set wildignore+=*.DS_Store "OSX SHIT"
set wildignore+=*.luac "Lua byte code"
set wildignore+=migrations "Django migrations"
set wildignore+=*.pyc "Python Object codes"
set wildignore+=*.orig "Merge resolution files"

"=------------------------------------------------------------------------------="
" => Motion Mappings
"=------------------------------------------------------------------------------="

" Navigations using keys up/down/left/right
" Enable if others are using my vim :(
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
nnoremap j gj
nnoremap k gk

" Navigation through windows
nnoremap <C-J> <C-W>j
nnoremap <C-K> <C-W>k
nnoremap <C-H> <C-W>h
nnoremap <C-L> <C-W>l
let g:BASH_Ctrl_j = 'off'

"=------------------------------------------------------------------------------="
" => Editing Mappings
"=------------------------------------------------------------------------------="

" Exit Insert mode
vnoremap <C-c> <Esc>
inoremap <C-c> <Esc>
inoremap jj <Esc>

" Add a backwards tab feature to complement <C-t>
inoremap <C-r> <C-d>

" Add a bash-like delete in insert mode 
inoremap <C-d> <C-o>x

" Re-hardwrap Paragraph
nnoremap <leader>q gqip

"Clipboard commands
map <leader>p "+p
map <leader>y "+yy

" Select just pasted text.
nnoremap <leader>v V`]

"=------------------------------------------------------------------------------="
" => Misc Mappings
"=------------------------------------------------------------------------------="

" Quick Save
inoremap <leader>w <C-O>:w!<CR>
noremap <leader>w :w!<CR>
" Clear all highlighting
nnoremap <leader><space> :noh<cr>

" Spelling (see General Settings for dict)
nnoremap <leader>s :set spell<CR>
nnoremap <leader>ns :set nospell<CR>

" Remove white space from a file.
nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>:noh<CR>

" ft Fold tag, helpful for HTML editing.
nnoremap <leader>ft vatzf

" Shortcut to edit .vimrc file
nnoremap <leader>ev <C-w><C-v><C-l>:e $MYVIMRC<cr>

" Get Rid of stupid Goddamned help keys
inoremap <F1> <ESC>
nnoremap <F1> <ESC>
vnoremap <F1> <ESC>

" Man pages in a seperate scratch file
runtime ftplugin/man.vim
if has("gui_running")
	nnoremap K :<C-U>exe "Man" v:count "<C-R><C-W>"<CR>
endif
"=------------------------------------------------------------------------------="
" => Personal Commands
"=------------------------------------------------------------------------------="
 
" LaTeX commands
command! Pdf !pdflatex % 
command! Bibi !bibtex % 
command! View !evince %<.pdf 

"=------------------------------------------------------------------------------="
" => Auto and FileType Commands
"=------------------------------------------------------------------------------="

" Set vim to save the file on focus out.
au FocusLost * :wa

" Auto Save
" TODO: Check file size? This can sometimes hang
" augroup autosave
"     autocmd CursorHold * nested update
" augroup END
" set updatetime=200

" Make Sure that Vim returns to the same line when we reopen a file"
augroup line_return
    au!
    au BufReadPost *
        \ if line("'\"") > 0 && line("'\"") <= line("$") |
        \ execute 'normal! g`"zvzz' |
        \ endif
augroup END

" Python Files
au FileType python setlocal expandtab shiftwidth=4 tabstop=8 softtabstop=4 smartindent
augroup vimrc_autocmds
    autocmd!
    " highlight characters past column 100
   autocmd FileType python highlight Excess ctermbg=DarkGrey guibg=#262626
    autocmd FileType python match Excess /\%100v.*/
    autocmd FileType python set nowrap
augroup END
 
" Latex Files
au FileType tex setlocal fo=cqt wm=0

"=------------------------------------------------------------------------------="
" => VIM Plugins (may contain settings, mappings, and autocommands)
"=------------------------------------------------------------------------------="

"" Remove those plugins that you do not want loaded
let g:pathogen_disabled = []
if !has('gui_running')
   call add(g:pathogen_disabled, 'tilda.vim')
endif
call pathogen#infect()

"----------------------
" Airline Powerline
"----------------------
"set rtp+=~/.vim/bundle/powerline/powerline/bindings/vim
"let g:airline_powerline_fonts = 1
"let g:airline_left_sep = ''
"let g:airline_right_sep = ''
"let g:airline_theme = 'powerlineish'
"let g:airline_detect_whitespace=0

"----------------------
" BufExplorer
"----------------------

let g:buffergator_hsplit_size = 8
let g:buffergator_vsplit_size = 8
let g:buffergator_viewport_split_policy = "T"
"let g:buffergator_display_regime = 'bufname'
let g:buffergator_autoexpand_on_split = 0
let g:buffergator_suppress_keymaps = 1
let g:buffergator_show_full_directory_path = 1
noremap <C-b> :BuffergatorToggle<CR>
noremap <C-TAB>   :BuffergatorMruCycleNext<CR>
noremap <C-S-TAB> :BuffergatorMruCyclePrev<CR>


"----------------------
" delimitMate 
"----------------------
" Make ctrl-Enter skip the delimiter
if has('gui_running')
   inoremap <C-CR> <C-R>=delimitMate#JumpAny()<CR>
else
   inoremap <NL> <C-R>=delimitMate#JumpAny()<CR>
endif
au FileType python let b:delimitMate_nesting_quotes = ['"', "'"]

"----------------------
" clang_complete
"----------------------
let g:clang_library_path="/usr/lib/llvm-3.5/lib/libclang.so"
let g:clang_complete_auto = 0
let g:clang_auto_select = 0
inoremap <silent> <buffer> <C-SPACE> <c-x><c-u>

"----------------------
" Guake
"----------------------
" TODO add this


"----------------------
" Gundo window 
"----------------------
map <leader>g :GundoToggle<CR>

"----------------------
" IndentLine 
"----------------------
let g:indentLine_bufNameExclude = ['_.*', 'NERD_tree.*']
let g:indentLine_fileTypeExclude = ['tex', 'txt']
let g:indentLine_color_term = 233
let g:indentLine_color_gui = '#41423c'
let g:indentLine_char = '¦'
autocmd BufEnter,FileReadPost * :IndentLinesReset

"----------------------
" Jedi Vim
"----------------------
let g:acp_enableAtStartup = 0
let g:jedi#popup_on_dot = 0
let g:jedi#popup_select_first = 0
let g:jedi#completions_enabled = 1
let g:jedi#auto_vim_configuration = 1
let g:jedi#show_call_signatures = 0

"----------------------
" MiniBuffer
"----------------------
"noremap <C-TAB>   :MBEbn<CR>
"noremap <C-S-TAB> :MBEbp<CR>
"" HACK: Tagbar and jedi conflict if a buffer is not opened (very odd)
"let g:miniBufExplBuffersNeeded = 1

"----------------------
" NERDTree
"----------------------
nnoremap <C-n> :NERDTreeToggle<cr>
let NERDTreeIgnore=['\.vim$', '\~$', '\.pyc$']

"----------------------
" Sign-Column Highlight
"----------------------
let g:abort_signcol_highlight = 0

"----------------------
" Supertab
"----------------------
let g:SuperTabDefaultCompletionType = "context"
let g:SuperTabContextDefaultCompletionType = "<c-n>"

"----------------------
" Syntastic
"----------------------
set statusline=%<\ \ %f\ \ %y\ %r\ %m\ %=%-23.(%#warningmsg#%{SyntasticStatuslineFlag()}%*\ \ \ \ Ln\ %l:\ %3c\ \ (%P)%) 
let g:syntastic_stl_format = '[%E{Errs: %e Ln: %fe}%B{, }%W{Warns: %w Ln: %fw}]'
let g:syntastic_python_python_exec='/usr/bin/env python'
let g:syntastic_python_checkers = ['flake8']
let g:syntastic_python_flake8_args = '--ignore="E501,E203,E302,E261,E701,E241,E126,E127,E128,W801"'
let g:syntastic_check_on_open=1
let g:syntastic_error_symbol = '•'
let g:syntastic_style_error_symbol = '•'
let g:syntastic_warning_symbol = '•'
let g:syntastic_style_warning_symbol = '•'

"----------------------
" Tagbar
"----------------------
let g:tagbar_sort = 0
nmap <leader>l <ESC>:TagbarToggle<cr>
imap <leader>l <ESC>:TagbarToggle<cr>i

"----------------------
" Tilda
"----------------------
let g:tilda_abort = 1

"----------------------
" Virtualenv
"----------------------
"TODO Add this plugin

"=------------------------------------------------------------------------------="
" => Color
"=------------------------------------------------------------------------------="

colorscheme badwolf
colorscheme mycolors
au VimLeave * !resetcolor

"=------------------------------------------------------------------------------="
" TODO: MOVE this to plugins
" => Python Virtualenv 
"=------------------------------------------------------------------------------="
if has('python') && strlen($VIRTUAL_ENV)
python << EOF
import os.path
import sys
import glob
import vim
import re
envdir = os.environ['VIRTUAL_ENV']
sys.path.insert(0, envdir)
activate_this = os.path.join(envdir, 'bin/activate_this.py')
exec(compile(open(activate_this, "rb").read(), activate_this, 'exec'), dict(__file__=activate_this))
#Remove site packages
for pattern in ('/usr/lib/python[\.0-9]+/dist-packages', 
                  '/usr/local/lib/python[\.0-9]+/(dist|site)-packages',
                  '/usr/lib/pymodules/python[\.0-9]+'):
   for path in sys.path[:]:
      if re.search(pattern, path):
         sys.path.pop(sys.path.index(path))
EOF
elseif has('python3') && strlen($VIRTUAL_ENV)
python3 << EOF
import os.path
import sys
import glob
import vim
import re
envdir = os.environ['VIRTUAL_ENV']
sys.path.insert(0, envdir)
activate_this = os.path.join(envdir, 'bin/activate_this.py')
exec(compile(open(activate_this, "rb").read(), activate_this, 'exec'), dict(__file__=activate_this))
#Remove site packages
for pattern in ('/usr/lib/python[\.0-9]+/dist-packages', 
                  '/usr/local/lib/python[\.0-9]+/(dist|site)-packages',
                  '/usr/lib/pymodules/python[\.0-9]+'):
   for path in sys.path[:]:
      if re.search(pattern, path):
         sys.path.pop(sys.path.index(path))
EOF
endif
" =========== END Add Python Virtaul Env  =========="
