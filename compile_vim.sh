# Compile vim 7.4+ for ubuntu 14.04
./configure --with-features=huge --enable-python3interp=yes --enable-perlinterp=yes --enable-luainterp=yes \
--enable-rubyinterp=yes --enable-tclinterp=yes --enable-gui --prefix=/usr

make VIMRCLOC=/usr/share/vim  VIMRUNTIMEDIR=/usr/share/vim/vim74  -j 4
