
if exists("g:guake_abort")
   finish
endif

let g:guake_index = 0

function! guake#start()
    let guake_tab_name = "VIM"
    let cwd = getcwd()
    call system("guake -n -")
    call system("guake -r ".guake_tab_name)
    call system('guake -e "cd '.cwd.'"')
    call system('guake -e "printf \"\033c"\"')
    let g:guake_index = system("guake -g")
endfunction

function! guake#end()
    call system("guake -s ".g:guake_index)
    call system("guake -e 'exit'")
endfunction

" function! guake#switch
" endfunction

call guake#start()
au VimLeavePre * call guake#end()
