
"=========================================="
"Sign-Column-Highlight
" ...ahh the little things in life :)
"=========================================="
 
if exists('g:abort_signcol_highlight') && (g:abort_signcol_highlight == 1)
   finish 
endif

" Current line sign name id (Identifier)
let g:curline_sign_id = 8888
let g:curline_sign_name = 'autosigncol_hl'

" Sign column dummy id (Identifier)
let g:auto_sign_id = 9999
let g:auto_sign_name = 'linesigncol_hl'

" HACK: Blank space are not allowed in the sign's text attribute 
" and it will take qutes as literal so I used the following unicode char:  
" 'text=<C-V>u00ad' (soft hyphen), aren't I clever? :)
let g:blank_space = '­'


" Setup the current line sign in appreciate buffers 
function! SignHLInit()
    " Looks terrible if there is no number line
    if (&number)
        " Always show the SignColumn
        execute 'sign define ' . g:auto_sign_name
        execute 'sign place '. g:auto_sign_id . ' line=1 name=' . g:auto_sign_name . ' buffer=' . bufnr('')
        " Current line sign information
        let b:current_line = -1
        let b:active_signs = []
        execute 'sign define ' . g:curline_sign_name . '  text=' . g:blank_space . ' texthl=CursorLineNr'
    endif
endfunction
autocmd BufReadPre,FileWritePre * call SignHLInit() 
"
"
"" Update all the active signs (excluding  auto_sign_name, curline_sign_name)
"function! UpdateActiveSigns()
"    let b:current_signs = {}
"    let b:current_linehl ={}
"    let b:current_texthl ={}
"    let b:current_text ={}
"
"    " NOTE: 'sign list' shows the texthl info but does so
"    " for ALL buffers, therefore we proceed in a roundabout
"    " way, first obtaining all signs in the current buffer
"    " using 'sign place buffer=' then we extract those extra
"    " names (not auto_sign_name, and curline_sign_name), and 
"    " then finally we determine all the texthl form 'sign list'
"
"    " All signs in the current buffer
"    redir => current_signs_str
"        silent execute "sign place buffer=" . bufnr('')
"    redir END
"
"    " Determine extra sign names and line numbers
"    for line in split(current_signs_str, '\n')
"        let name_array = split(matchstr(line, 'name=[0-9a-zA-Z_]*'), '=')
"        if len(name_array) > 0
"            if (name_array[1] != g:curline_sign_name) && (name_array[1] != g:auto_sign_name)
"                let id = split(matchstr(line, 'line=[0-9]*'), '=')[1]
"                let b:current_signs[id] = name_array[1]
"            endif
"        endif
"    endfor
"
"    " All signs information
"    redir => signs_list_str
"        silent execute "sign list"
"    redir END
"
"    " Determine texthl linehl text information for the current_signs 
"    for line in split(signs_list_str, '\n')
"        for sign in values(b:current_signs)
"            if (match(line, 'sign ' . sign) >= 0)
"                let texthl = split(matchstr(line, 'texthl=[0-9a-zA-Z_]*'), '=')[1]
"                let linehl = split(matchstr(line, 'linehl=[0-9a-zA-Z_]*'), '=')[1]
"                let text = split(matchstr(line, 'text=.'), '=')[1]
"                if (!has_key(b:current_text, text))
"                    let b:current_texthl[sign] = texthl
"                    let b:current_linehl[sign] = linehl
"                    let b:current_text[sign] = text
"                endif
"            endif
"        endfor
"    endfor
"    echo b:current_signs b:current_text b:current_texthl b:current_linehl
"
"endfunction
"autocmd BufWritePost,FileWritePost * call UpdateActiveSigns()
"
"
"" Highlight the current line's sign with the same color as SignColumn
"function! SignHLFollow()
"    " Do Not Execute IF: 1) this buffer does not have a local current_line 
"    " variable and 2) we have only moved horizontally (current_line unchanged)
"    if exists('b:current_line') && (b:current_line != line('.'))
"        let b:current_line = line('.')
"        execute 'sign unplace ' . g:curline_sign_id . ' buffer=' . bufnr('')
"        execute 'sign place ' . g:curline_sign_id . ' line=' . b:current_line . ' name=' . g:curline_sign_name . ' buffer=' . bufnr('')
"    endif
"endfunction
"autocmd BufReadPre,CursorMovedI,CursorMoved * :call SignHLFollow()

